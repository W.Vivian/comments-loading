let xhr = new XMLHttpRequest()
console.log('sent http')
let baseUrl = 'https://jsonplaceholder.typicode.com/comments'
console.log('sent http done')
const initialSize = 30
const loadSize = 10
let currentSize = initialSize
let i = 1
let eleLoading = document.getElementById('loading')
let content = document.getElementById('commentArea')
let initialTop = content.scrollTop
let initialHeight = content.scrollHeight
let flagLoading = true
let loadingDone = false

function addText(comments) {
    content.value += 'NO. ' + comments.id + '\n' + 'Name: ' + comments.name + '\n' + comments.body + '\n\n'
    if (i < currentSize && loadingDone === false) {
        // content.value += 'NO. ' + comments.id + '\n' + 'Name: ' + comments.name + '\n' + comments.body + '\n\n'

        sendXHR(++i)
    } else if (i === currentSize) {
        console.log('loading done')
        loadingDone = true
    }

}

function sendXHR(id) {
    let url = id > 0 ? `${baseUrl}/${id}` : baseUrl
    xhr.open('GET', url)
    setTimeout(_ => {
        flagLoading ? xhr.send() : _
        flagLoading = false
        eleLoading.style.visibility = flagLoading ? 'visible' : 'hidden'
    }, 0)
}

setTimeout(() => sendXHR(i), 1000)

console.log('initial top is : ' + content.scrollTop)
console.log('initial height is : ' + content.scrollHeight)

content.onscroll = (e) => {
    console.log('onscroll[', e)
    if (loadingDone === true) {
        console.log('loadingDone[', loadingDone)
        scrollMore(e)
    }
}

function scrollMore(e) {
    console.log(`Scroll position: ${e.target.scrollTop}`)
    if (e.target.scrollTop - initialTop === e.target.scrollHeight - initialHeight) {
        currentSize += loadSize
        eleLoading.style.visibility = flagLoading ? 'visible' : 'hidden'
        loadingDone = false
        i++
        setTimeout(() => sendXHR(i), 1000)
        initialTop = e.target.scrollTop
        initialHeight = e.target.scrollHeight
    }
}

xhr.onload = () => {
    flagLoading = true
    //loadingDone = true
    console.log('sent http responded')
    let comments = JSON.parse(xhr.response)
    console.log('The request is finished')
    addText(comments)

}

xhr.onerror = () => {
    eleLoading.style.visibility = flagLoading ? 'visible' : 'hidden'
    console.log('error in http request')
}